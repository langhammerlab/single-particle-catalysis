#%%Plot figure 2
import numpy as np
from matplotlib.pyplot import get_cmap
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.colors import Normalize
from matplotlib.cm import ScalarMappable
from matplotlib.patches import Rectangle
import seaborn as sns
from labellines import labelLines
from Scripts.utils import *

sns.set(font_scale=1.5,style="white")

Pd1000QMS = np.load("Results/Pd1000-QMS.npy")
Pd1000QMSFull = np.load("Results/Pd1000-QMS-Full.npy")
Pd10QMSFull = np.load("Results/Pd10-QMS-Full.npy")
Pd10QMS = np.load("Results/Pd10-QMS.npy")

x1=265
x2=3222
titleFS=24
pd10C = get_cmap("Greens")
pd1000C = get_cmap("Blues")
c1 = 0.95
c2 = 0.65
c3 = 0.35

PO = [0,0.75,0.8]
PO = np.append(PO,np.arange(0.9,6.3,0.3))
PCO = np.flip(np.append([0,0.4],np.arange(0.6,6.3,0.3)))
PO = np.append(PO,np.flip(PO))
PCO = np.append(PCO,np.flip(PCO))
alpha = PCO/(PO+PCO)
alpha=np.append(0.5,alpha)

QMSInputCO=np.loadtxt("scripts/GasInput.txt")
partIndicesOfGas = np.loadtxt("scripts/GasIndices.txt",dtype=int)
partIndicesOfNoGas = np.loadtxt("scripts/ArIndices.txt",dtype=int)
QMSCOO2=getQMSInput(QMSInputCO,partIndicesOfGas,partIndicesOfNoGas)

QMSO = QMSCOO2[1,x1:x2]
QMSCO = QMSCOO2[0,x1:x2]
xtick= np.linspace(0,1,len(QMSO))

yAxs = 4
xAxs=3

fig = plt.figure(figsize=(16, 16)) # Adjust size as needed

# Add a small header with 3 subplots
header_ax = [None] * yAxs
for i in range(xAxs):
    header_ax[i] = fig.add_subplot(yAxs, xAxs, (i+1,i+1))
    plt.plot(xtick,QMSO,label='CO',c="black",alpha=0.7)
    plt.plot(xtick,QMSCO,label='$O_2$',c="red",alpha=0.7)  
    if i==0:
     plt.legend(loc="upper left")
    sns.despine(left=True, bottom=True)
    header_ax[i].set(xticklabels=[])
    header_ax[i].set(ylabel="Gas Conc. (%)")
    
header_ax[0].set_title("450$^o$C",fontsize=titleFS)
header_ax[1].set_title("340$^o$C",fontsize=titleFS)
header_ax[2].set_title("280$^o$C",fontsize=titleFS)
ax2 = fig.add_subplot(yAxs, xAxs, 4)
ax2.plot(xtick,Pd1000QMSFull[1,x1:x2],c=pd1000C(c1),label="$CO_2$ QMS")
plt.legend(loc="upper left")
ax2.set_title("1000 Pd NP, 450$^o$C")
ax2.set(xticklabels=[])
ax2.set(ylabel="BA-Counts (a.u.)")

ax3 = fig.add_subplot(yAxs, xAxs, 5)
ax3.plot(xtick,Pd1000QMSFull[13,x1:x2],c=pd1000C(c2))
ax3.set_title("1000 Pd NP, 340$^o$C")
ax3.set(xticklabels=[])
ax3.set(ylabel="BA-Counts (a.u.)")

ax4 = fig.add_subplot(yAxs, xAxs, 6)
ax4.plot(xtick,Pd1000QMSFull[-2,x1:x2],c=pd1000C(c3))
ax4.set_title("1000 Pd NP, 280$^o$C")
ax4.set(xticklabels=[])
ax4.set(ylabel="BA-Counts (a.u.)")

ax5 = fig.add_subplot(yAxs, xAxs, 7)
ax5.plot(xtick,Pd10QMSFull[1,x1:x2],c=pd10C(c1),label="$CO_2$ QMS")
plt.legend(loc="upper left")
ax5.set_title("10 Pd NP, 450$^o$C")
ax5.set(xticklabels=[])
ax5.set(ylabel="BA-Counts (a.u.)")

ax6 = fig.add_subplot(yAxs, xAxs, 8)
ax6.plot(xtick,Pd10QMSFull[11,x1:x2],c=pd10C(c2))
ax6.set_title("10 Pd NP, 340$^o$C")
ax6.set(xticklabels=[])
ax6.set(ylabel="BA-Counts (a.u.)")

ax7 = fig.add_subplot(yAxs, xAxs, 9)
ax7.plot(xtick,Pd10QMSFull[-2,x1:x2],c=pd10C(c3))
ax7.set_title("10 Pd NP, 280$^o$C")
ax7.set(xticklabels=[])
ax7.set(ylabel="BA-Counts (a.u.)")

ax8 = fig.add_subplot(yAxs, xAxs, 10)
lin1=ax8.scatter(np.flip(alpha[1:22]),Pd1000QMS[1,1:22],c=pd1000C(c1),label="1000 Pd NP")
fit = RollQMS(Pd1000QMS[1,1:22])
xRange=np.linspace(0,1,len(fit))

maxX=np.flip(alpha[1:22])[np.argmax(Pd1000QMS[1,1:22])]
maxY=np.max(Pd1000QMS[1,1:22])
minY=np.min(Pd1000QMS[1,1:22])
maxX=xRange[np.argmax(fit)]
maxY=np.max(fit)
minY=0

ax8.set(ylabel="Counts, 1000 NP")
ax8.yaxis.label.set_color(pd1000C(c1))
ax8.set(xlabel="$α_{CO}=P_{CO}/(P_{O2}+P_{CO})$")

twin_ax8 = ax8.twinx() 
lin2=twin_ax8.scatter(np.flip(alpha[1:22]),Pd10QMS[1,1:22],c=pd10C(c1),label="10 Pd NP",marker="x")

Pd10QMSSTD=[getAlphaValue(Pd10QMSFull[i,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff=10,std=1) for i in range(0,len(Pd10QMSFull))]
twin_ax8.fill_between(np.flip(alpha[1:22]),Pd10QMS[1,1:22]-Pd10QMSSTD[1][1:22],Pd10QMS[1,1:22]+Pd10QMSSTD[1][1:22],color=pd10C(c1),alpha=0.3)
Pd1000QMSSTD=[getAlphaValue(Pd1000QMSFull[i,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff=10,std=1) for i in range(0,len(Pd1000QMSFull))]
ax8.fill_between(np.flip(alpha[1:22]),Pd1000QMS[1,1:22]-Pd1000QMSSTD[1][1:22],Pd1000QMS[1,1:22]+Pd1000QMSSTD[1][1:22],color=pd1000C(c1),alpha=0.3)
fit = RollQMS(Pd10QMS[1,1:22])
xRange=np.linspace(0,1,len(fit))

twin_ax8.set(ylabel="Counts, 10 NP")
twin_ax8.yaxis.label.set_color(pd10C(c1))
twin_ax8.set_xticklabels(np.flip(twin_ax8.get_xticklabels()))
maxX=xRange[np.argmax(fit)]
maxY=np.max(fit)
minY=0

lns = [lin1,lin2]
labs = [l.get_label() for l in lns]
ax8.legend(lns, labs, loc="best")

ax9 = fig.add_subplot(yAxs, xAxs, 11)
lin1=ax9.scatter(np.flip(alpha[1:22]),Pd1000QMS[13,1:22],c=pd1000C(c2),label="1000 Pd NP")
fit = RollQMS(Pd1000QMS[13,1:22])
xRange=np.linspace(0,1,len(fit))

maxX=np.flip(alpha[1:22])[np.argmax(Pd1000QMS[13,1:22])]
maxY=np.max(Pd1000QMS[13,1:22])
minY=np.min(Pd1000QMS[13,1:22])
maxX=xRange[np.argmax(fit)]
maxY=np.max(fit)
minY=0

ax9.set(ylabel="Counts, 1000 NP")
ax9.yaxis.label.set_color(pd1000C(c2))
ax9.set(xlabel="$α_{CO}=P_{CO}/(P_{O2}+P_{CO})$")
twin_ax9 = ax9.twinx() 
lin2=twin_ax9.scatter(np.flip(alpha[1:22]),Pd10QMS[11,1:22],c=pd10C(c2),label="10 Pd NP",marker="x")

fit = RollQMS(Pd10QMS[11,1:22])
xRange=np.linspace(0,1,len(fit))
    
twin_ax9.set(ylabel="Counts, 10 NP")
twin_ax9.yaxis.label.set_color(pd10C(c2))
twin_ax9.fill_between(np.flip(alpha[1:22]),Pd10QMS[11,1:22]-Pd10QMSSTD[11][1:22],Pd10QMS[11,1:22]+Pd10QMSSTD[11][1:22],color=pd10C(c2),alpha=0.3)
ax9.fill_between(np.flip(alpha[1:22]),Pd1000QMS[13,1:22]-Pd1000QMSSTD[13][1:22],Pd1000QMS[13,1:22]+Pd1000QMSSTD[13][1:22],color=pd1000C(c2),alpha=0.3)

maxX=np.flip(alpha[1:22])[np.argmax(Pd10QMS[11,1:22])]
maxY=np.max(Pd10QMS[11,1:22])
minY=np.min(Pd10QMS[11,1:22])
maxX=xRange[np.argmax(fit)]
maxY=np.max(fit)
minY=0

twin_ax9.set_xticklabels(np.flip(twin_ax9.get_xticklabels()))
ax10 = fig.add_subplot(yAxs, xAxs, 12)
lin1=ax10.scatter(np.flip(alpha[1:22]),Pd1000QMS[-2,1:22],c=pd1000C(c3),label="1000 Pd NP")

fit = RollQMS(Pd1000QMS[-2,1:22])
xRange=np.linspace(0,1,len(fit))
    
maxX=np.flip(alpha[1:22])[np.argmax(Pd1000QMS[-2,1:22])]
maxY=np.max(Pd1000QMS[-2,1:22])
minY=np.min(Pd1000QMS[-2,1:22])
maxX=xRange[np.argmax(fit)]
maxY=np.max(fit)
minY=0
ax10.set(ylabel="Counts, 1000 NP")
ax10.yaxis.label.set_color(pd1000C(c3))
ax10.set(xlabel="$α_{CO}=P_{CO}/(P_{O2}+P_{CO})$")
twin_ax10 = ax10.twinx() 
lin2=twin_ax10.scatter(np.flip(alpha[1:22]),Pd10QMS[-2,1:22],c=pd10C(c3),label="10 Pd NP",marker="x")
twin_ax10.fill_between(np.flip(alpha[1:22]),Pd10QMS[-2,1:22]-Pd10QMSSTD[-2][1:22],Pd10QMS[-2,1:22]+Pd10QMSSTD[-2][1:22],color=pd10C(c3),alpha=0.3)
ax10.fill_between(np.flip(alpha[1:22]),Pd1000QMS[-2,1:22]-Pd1000QMSSTD[-2][1:22],Pd1000QMS[-2,1:22]+Pd1000QMSSTD[-2][1:22],color=pd1000C(c3),alpha=0.3)
fit = RollQMS(Pd10QMS[-2,1:22])
xRange=np.linspace(0,1,len(fit))
    
twin_ax10.set(ylabel="Counts, 10 NP")
twin_ax10.yaxis.label.set_color(pd10C(c3))
maxX=np.flip(alpha[1:22])[np.argmax(Pd10QMS[-2,1:22])]
maxY=np.max(Pd10QMS[-2,1:22])
minY=np.min(Pd10QMS[-2,1:22])
maxX=xRange[np.argmax(fit)]
maxY=np.max(fit)
minY=0

twin_ax10.set_xticklabels(np.flip(twin_ax10.get_xticklabels()))

x1=-100
x2=5000
ax2.set_ylim(x1,x2)
ax3.set_ylim(x1,x2)
ax4.set_ylim(x1,x2)
x2=200
ax5.set_ylim(x1,x2)
ax6.set_ylim(x1,x2)
ax7.set_ylim(x1,x2)

x1=-0.32
y1=1.03
header_ax[0].annotate("A", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
header_ax[1].annotate("B", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
header_ax[2].annotate("C", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2.annotate("D", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax3.annotate("E", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax4.annotate("F", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax5.annotate("G", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax6.annotate("H", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax7.annotate("I", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax8.annotate("J", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax9.annotate("K", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax10.annotate("L", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)

# Adjust subplot spacing
fig.subplots_adjust(left=0.075, bottom=0.1, right=0.95, top=0.95, wspace=0.5, hspace=0.2)


#%% Figure 4
norm = Normalize(vmin=c3, vmax=c1)
x1=265
x2=3222
pd10DAEC = get_cmap("Oranges")

temperatures=np.loadtxt("Results/Pd1000-Temps.npy",dtype=int)
Pd10Pred1 = np.load("Results/Pd10-Pred1.npy")
Pd10Pred2 = np.load("Results/Pd10-Pred2.npy")
Pd10PredFull = np.load("Results/Pd10-PredFull.npy")
Pd10Temperatures=np.load("Results/Pd10-Temps.npy")

indicesCutOff=3

fig=plt.figure(figsize=(16,16))
xAxs=3
yAxs=8
gs = gridspec.GridSpec(yAxs, xAxs)

a=0.5
s=128
ylim=10000

saveMainPredX=[]
saveMainPredY=[]

marker="o"
ax1H=plt.subplot(gs[0,0])
ax1H.plot(Pd1000QMSFull[0,x1:x2],c="black",alpha=a)
ax1H.plot(getMeanValue(Pd1000QMSFull[0,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff)[x1:x2],c=pd1000C(c1),label="460$^oC$")
ax1H.text(0.01,3000,"450$^oC$",c=pd1000C(c1))

ax1H.set(xticks=[])
ax1L=plt.subplot(gs[1,0])
xRange=np.linspace(0,1,x2-x1)   
ax1L.plot(xRange,Pd1000QMSFull[-1,x1:x2],c="black",label="280$^oC$",alpha=a)
ax1L.plot(xRange,getMeanValue(Pd1000QMSFull[-1,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff)[x1:x2],c=pd1000C(c3))
ax1L.set(xticks=[])
ax1=plt.subplot(gs[2:-2,0])
ax1H.set_title("Pd1000",fontsize=titleFS)
ax1L.text(0.01,1250,"280$^oC$",c=pd1000C(c3))

for i in range(0,len(Pd1000QMS),2):
    temp=temperatures[i]
    lab=None
    if i==0 or i==len(Pd1000QMS)-1:
        lab=str(temp)+"$^oC$"
    
    QMSValMean = Pd1000QMS[i,1:22]

    ax1.plot(np.flip(alpha[1:22]),QMSValMean,c=pd1000C(c1-i*(c1-c3)/len(Pd1000QMS)),label=lab,alpha=a)
    ax1.scatter(1-alpha[np.argmax(QMSValMean)+1],np.max(QMSValMean),c="red",alpha=a,s=s,marker=marker)
    saveMainPredX=np.append(saveMainPredX,1-alpha[np.argmax(QMSValMean)+1])
    saveMainPredY=np.append(saveMainPredY,np.max(QMSValMean))
    
sm = ScalarMappable(cmap=pd1000C, norm=norm)
sm.set_array([]) 
axins = ax1.inset_axes([0.78, 0.63, 0.05, 0.3])
bar = plt.colorbar(sm, cax=axins, orientation='vertical')
tick_positions = np.linspace(c3, c1, 10)  # 18 ticks from 0.35 to 0.95
tick_labels = np.flip(temperatures[::2])  # Corresponding temperature labels from 280 to 450
bar.set_ticks(tick_positions)
bar.set_ticklabels(tick_labels)
for label_index, label in enumerate(bar.ax.yaxis.get_ticklabels()):
    if label_index % 3:
            label.set_visible(False)

ax1.set_ylim(0,ylim)
ax1.set(xticks=[])

ax1Max=plt.subplot(gs[-2:,0])
ax1Max.set_xlabel(r'$α_{CO}$')
ax1Max.set_ylabel("T ($^oC$)")
ax1Max.scatter(saveMainPredX,temperatures[::2],c="red",alpha=a,s=s,marker=marker,label=r"$\alpha_{CO}^{max}$")
ax1Max.set_ylim(260,470)
ax1Max.set_xlim(0,1)
ax1Max.grid(axis="both")
ax1Max.set_xticklabels(np.flip(ax1Max.get_xticklabels()))
ax1Max.legend(loc="best")
maxAlphaPd1000 = np.copy(saveMainPredX)
maxPd1000 = np.copy(saveMainPredY)

saveMainPredX=[]
saveMainPredY=[]

ax2H=plt.subplot(gs[0,1])
ax2L=plt.subplot(gs[1,1])
xRange=np.linspace(0,1,x2-x1)
ax2H.plot(Pd10QMSFull[0,x1:x2],c="black",label="460$^oC$",alpha=a)
ax2H.plot(getMeanValue(Pd10QMSFull[0,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff)[x1:x2],c=pd10C(c1))
ax2H.text(0.01,175,"450$^oC$",c=pd10C(c1))
ax2L.plot(xRange,Pd10QMSFull[-1,x1:x2],c="black",label="280$^oC$",alpha=a)
ax2L.plot(xRange,getMeanValue(Pd10QMSFull[-1,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff)[x1:x2],c=pd10C(c3))
ax2=plt.subplot(gs[2:-2,1])
ax2H.set(xticks=[])

ax2L.set(xticks=[])
ax2H.set_title("Pd10",fontsize=titleFS)
ax2L.text(0.01,75,"280$^oC$",c=pd10C(c3))

for i in range(0,len(Pd10QMS)):
    temp=Pd10Temperatures[i]
    lab=None
    if i==0 or i==len(Pd10QMS)-1:
        lab=str(temp)+"$^oC$"
    
    QMSValMean = Pd10QMS[i,1:22]
    QMSValMean=getAlphaValue(Pd10QMSFull[i,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff=indicesCutOff)[1:22]
    ax2.plot(np.flip(alpha[1:22]),QMSValMean,c=pd10C(c1-i*(c1-c3)/len(Pd10QMS)),label=lab,alpha=a)
    ax2.scatter(1-alpha[np.argmax(QMSValMean)+1],np.max(QMSValMean),c="red",alpha=a,s=s,marker=marker)
    saveMainPredX=np.append(saveMainPredX,1-alpha[np.argmax(QMSValMean)+1])
    saveMainPredY=np.append(saveMainPredY,np.max(QMSValMean))

sm = ScalarMappable(cmap=pd10C, norm=norm)
sm.set_array([]) 
axins = ax2.inset_axes([0.78, 0.63, 0.05, 0.3])
bar = plt.colorbar(sm, cax=axins, orientation='vertical')
tick_positions = np.linspace(c3, c1, len(Pd10QMS))  # 18 ticks from 0.35 to 0.95
tick_labels = np.flip(Pd10Temperatures).astype(int)  # Corresponding temperature labels from 280 to 450
bar.set_ticks(tick_positions)
bar.set_ticklabels(tick_labels)
for label_index, label in enumerate(bar.ax.yaxis.get_ticklabels()):
    if label.get_text() != "280" and label.get_text() != "450" and label.get_text() != "340" and label.get_text() != "400":  # Hide every other label
        label.set_visible(False)    

ax2.set_ylim(0,int(ylim/100))
ax2.set(xticks=[])

ax2Max=plt.subplot(gs[-2:,1])
ax2Max.set_xlabel(r'$α_{CO}$')
ax2Max.set_ylabel("T ($^oC$)")
ax2Max.scatter(saveMainPredX,Pd10Temperatures,c="red",alpha=a,s=s,marker=marker,label=r"$\alpha_{CO}^{max}$")
ax2Max.set_ylim(260,470)
ax2Max.set_xlim(0,1)
ax2Max.grid(axis="both")
ax2Max.legend(loc="best")
ax2Max.set_xticklabels(np.flip(ax2Max.get_xticklabels()))
saveMainPredX=[]
saveMainPredY=[]

ax3H=plt.subplot(gs[0,2])
ax3L=plt.subplot(gs[1,2])
xRange=np.linspace(0,1,x2-x1)
ax3H.plot(Pd10QMSFull[0,x1:x2],c="black",label="450$^oC$",alpha=a)
ax3H.plot(Pd10PredFull[0,x1:x2],c=pd10DAEC(c1),alpha=1)
ax3L.plot(xRange,Pd10QMSFull[-1,x1:x2],c="black",label="280$^oC$",alpha=a)
ax3L.plot(xRange,Pd10PredFull[-1,x1:x2],c=pd10DAEC(c3),alpha=1)  
ax3L.set(xticks=[])
ax3H.text(0.01,175,"450$^oC$",c=pd10DAEC(c1))#,fontsize=titleFS)
ax3=plt.subplot(gs[2:-2,2])
ax3L.text(0.01,75,"280$^oC$",c=pd10DAEC(c3))#,fontsize=titleFS)

ax3H.set(xticks=[])
ax3.set(xticks=[])
ax3H.set_title("Pd10 DAE",fontsize=titleFS)
for i in range(0,len(Pd10QMS)):
    temp=Pd10Temperatures[i]
    lab=None
    if i==0 or i==len(Pd10QMS)-1:
        lab=str(temp)+"$^oC$"
    
    QMSVal =(Pd10Pred1[i,1:22]+Pd10Pred2[i,1:22])/2
    QMSValStd = np.abs(Pd10Pred2[i,1:22]-Pd10Pred1[i,1:22])/2

    QMSAlt=Pd10Pred2[i,1:22]
    QMSMain=Pd10Pred1[i,1:22]
        
    QMSVal =(QMSAlt+QMSMain)/2
    QMSValStd = np.abs(QMSAlt-QMSMain)/2

    ax3.plot(np.flip(alpha[1:22]),QMSMain,c=pd10DAEC(c1-i*(c1-c3)/len(Pd1000QMS)),label=lab,alpha=a)
    ax3.scatter(1-alpha[np.argmax(QMSMain)+1],np.max(QMSMain),c="red",alpha=a,s=s,marker=marker)
    saveMainPredX=np.append(saveMainPredX,1-alpha[np.argmax(QMSMain)+1])
    saveMainPredY=np.append(saveMainPredY,np.max(QMSMain))

sm = ScalarMappable(cmap=pd10DAEC, norm=norm)
sm.set_array([]) 
axins = ax3.inset_axes([0.78, 0.63, 0.05, 0.3])
bar = plt.colorbar(sm, cax=axins, orientation='vertical')
tick_positions = np.linspace(c3, c1, len(Pd10QMS))  # 18 ticks from 0.35 to 0.95
tick_labels = np.flip(Pd10Temperatures).astype(int)  # Corresponding temperature labels from 280 to 450
bar.set_ticks(tick_positions)
bar.set_ticklabels(tick_labels)
for label_index, label in enumerate(bar.ax.yaxis.get_ticklabels()):
    if label.get_text() != "280" and label.get_text() != "450" and label.get_text() != "340" and label.get_text() != "400":  # Hide every other label
        label.set_visible(False)

ax3.set_ylim(0,int(ylim/100))

ax3Max=plt.subplot(gs[-2:,2])
ax3Max.set_xlabel(r'$α_{CO}$')
ax3Max.set_ylabel("T ($^oC$)")
ax3Max.scatter(saveMainPredX,Pd10Temperatures,c="red",alpha=a,s=s,marker=marker,label=r"$\alpha_{CO}^{max}$")
ax3Max.set_ylim(260,470)
ax3Max.set_xlim(0,1)
ax3Max.legend(loc="best")
ax3Max.grid(axis="both")

ax3Max.set_xticklabels(np.flip(ax3Max.get_xticklabels()))

fig.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=0.35, hspace=0.3)
maxAlphaPd10 = np.copy(saveMainPredX)
maxPd10 = np.copy(saveMainPredY)

ax1.set_ylabel("BA-Counts (a.u.)")
ax2.set_ylabel("BA-Counts (a.u.)")
ax3.set_ylabel("BA-Counts (a.u.)")

x1=-0.32
y1=1

ax1H.annotate("A", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2H.annotate("B", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax3H.annotate("C", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)

ax1L.annotate("D", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2L.annotate("E", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax3L.annotate("F", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)

ax1.annotate("G", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2.annotate("H", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax3.annotate("I", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)

ax1Max.annotate("J", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2Max.annotate("K", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax3Max.annotate("L", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
#%% Figure 5
x1=265
x2=3222

alphaX=22

Pd1HTQMSFull  = np.load("Results/Pd1-QMS-Full.npy")
Pd1HTQMS  = np.load("Results/Pd1-QMS.npy")
Pd1HTPred1 = np.load("Results/Pd1-Pred1.npy")
Pd1HTPred2 = np.load("Results/Pd1-Pred2.npy")
Pd1HTTemps = np.load("Results/Pd1-Temps.npy")

Pd0HTQMSFull  = np.load("Results/Pd0-QMS-Full.npy")
Pd0HTQMS  = np.load("Results/Pd0-QMS.npy")
Pd0HTPred1 = np.load("Results/Pd0-Pred1.npy")
Pd0HTPred2 = np.load("Results/Pd0-Pred2.npy")
Pd0HTTemps = np.load("Results/Pd0-Temps.npy")

PO = [0,0.75,0.8]
PO = np.append(PO,np.arange(0.9,6.3,0.3))
PCO = np.flip(np.append([0,0.4],np.arange(0.6,6.3,0.3)))
PO = np.append(PO,np.flip(PO))
PCO = np.append(PCO,np.flip(PCO))
alpha = PCO/(PO+PCO)
alpha=np.append(0.5,alpha)
alphaPlot = alpha[1:alphaX]

def GetQMSPredFromValues(values,partIndicesOfGas,partIndicesOfNoGas): #replace this with actual predictions? should be the same
    QMS = np.zeros(len(Pd1HTQMSFull[0,:]))
    for i in range(0,np.size(partIndicesOfNoGas)-1):
        QMS[partIndicesOfGas[i]:partIndicesOfNoGas[i+1]] = values[i]   
        QMS[partIndicesOfNoGas[i]:partIndicesOfGas[i]] = 0  
    QMS[partIndicesOfNoGas[-1]:] = 0
    return QMS

pd10DAEC = get_cmap("Oranges")
pd1DAEC = get_cmap("Purples")
pd10C = get_cmap("Greens")

indicesCutOff=3
fig=plt.figure(figsize=(16,16))
xAxs=2
yAxs=10
gs = gridspec.GridSpec(yAxs, xAxs)

c3=0.05
a=0.5
s=128
ylim=10000
rollingWindow=0

saveMainPredX=[]
saveMainPredY=[]

ax1H1=plt.subplot(gs[0,0])
ax1L1=plt.subplot(gs[1,0])
xRange=np.linspace(0,1,x2-x1)
ax1H1.plot(Pd1HTQMSFull[1,x1:x2],c="black",label="460$^oC$",alpha=a)
ax1H1.plot(getMeanValue(Pd1HTQMSFull[1,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff)[x1:x2],c=pd10C(0.9))
ax1L1.plot(xRange,Pd0HTQMSFull[3,x1:x2],c="black",label="280$^oC$",alpha=a)
ax1L1.plot(xRange,getMeanValue(Pd0HTQMSFull[3,:],partIndicesOfGas,partIndicesOfNoGas,indicesCutOff)[x1:x2],c="black")

ax1=plt.subplot(gs[2:-2,0])
ax1H1.set(xticks=[])
ax1L1.set(xticks=[])

ax1H1.set_title("Pd1/Pd0",fontsize=titleFS)
ax1H1.set_yticks([0,100])
ax1L1.set_yticks([0,100])
ax1H1.text(-120,35,"450$^oC$",c=pd10C(c1))
ax1L1.text(-0.04,35,"450$^oC$",c="black")
ax1.set_ylabel("BA-Counts (a.u.)")

saveMainPredX=[]
saveMainPredY=[]
for i in range(2,len(Pd1HTQMS)):
    temp=Pd1HTQMS[i]
    lab=None
    if i==2 :
        lab="$450^oC$"
    elif i==len(Pd1HTQMS)-1:
        lab="$280^oC$"
    QMSValMean = Pd1HTQMS[i,1:alphaX]

    ax1.plot(np.flip(alpha[1:alphaX]),QMSValMean,c=pd10C(c1-i*(c1-c3)/len(Pd1HTQMS)),label=lab,alpha=1)
    saveMainPredX=np.append(saveMainPredX,1-alpha[np.argmax(QMSValMean)+1])
    saveMainPredY=np.append(saveMainPredY,Pd1HTTemps[i])
   
ax1.set_xlabel(r'$α_{CO}=P_{CO}/(P_{O2}+P_{CO})$')
ax1.set_xticklabels(np.flip(ax1.get_xticklabels())) 


for i in range(0,len(Pd0HTQMS)):

    temp=Pd0HTQMS[i]
    lab=None
    if i==0 :
        lab="$450^oC$"
    
    QMSValMean = Pd0HTQMS[i,1:alphaX]
    ax1.plot(np.flip(alpha[1:alphaX]),QMSValMean,c="black",label=lab,alpha=1)

ax1.set_ylim(-10,70)

sm = ScalarMappable(cmap=pd10C, norm=norm)
sm.set_array([]) 
axins = ax1.inset_axes([0.78, 0.63, 0.05, 0.3])
bar = plt.colorbar(sm, cax=axins, orientation='vertical')
tick_positions = np.linspace(c3, c1, 10)  # 18 ticks from 0.35 to 0.95
tick_labels = np.flip(temperatures[::2])  # Corresponding temperature labels from 280 to 450
bar.set_ticks(tick_positions)
bar.set_ticklabels(tick_labels)
for label_index, label in enumerate(bar.ax.yaxis.get_ticklabels()):
    if label_index % 3:
        label.set_visible(False)

l1=ax1.plot(0,-100,c=pd10C(c2),label="Pd1 Mean")
l2=ax1.plot(0,-100,c="black",label="Pd0 Mean")
handles,labels=ax1.get_legend_handles_labels()
ax1.legend(handles[-2:],labels[-2:],loc="upper left")

ax2H1=plt.subplot(gs[0,1])
ax2L1=plt.subplot(gs[1,1])
xRange=np.linspace(0,1,x2-x1)
ax2H1.plot(Pd1HTQMSFull[1,x1:x2],c="black",label="460$^oC$",alpha=a)
ax2H1.plot(GetQMSPredFromValues(Pd1HTPred1[1],partIndicesOfGas,partIndicesOfNoGas)[x1:x2],c=pd1DAEC(0.9))
ax2L1.plot(xRange,Pd0HTQMSFull[3,x1:x2],c="black",label="280$^oC$",alpha=a)
ax2L1.plot(xRange,GetQMSPredFromValues(Pd0HTPred1[3],partIndicesOfGas,partIndicesOfNoGas)[x1:x2],c="black")
ax2=plt.subplot(gs[2:-2,1])
ax2H1.set(xticks=[])
ax2L1.set(xticks=[])

ax2H1.set_title("Pd1/Pd0 DAE",fontsize=titleFS)
ax2H1.set_yticks([0,100])
ax2L1.set_yticks([0,100])
ax2H1.text(-120,35,"450$^oC$",c=pd1DAEC(c1))#,fontsize=titleFS)
ax2L1.text(-0.04,35,"450$^oC$",c="black")#,fontsize=titleFS)
ax2.set_ylabel("BA-Counts (a.u.)")

ax2Max=ax2.inset_axes([0.75,0.75,0.2,0.2])
ax2Max.set_xlabel(r'$α$')
ax2Max.set_ylabel("T ($^oC$)")

ax2Max.set_ylim(260,470)
ax2Max.set_xlim(0,1)
ax2Max.set_xticklabels(np.flip(ax2Max.get_xticklabels())) 
ax2Max.grid(axis="both")
    
saveMainPredX=[]
saveMainPredY=[]

inds = [2,4,6,8,10,12,14] #Use every other temperature for article, to reduce clutter. 
inds=np.arange(0,len(Pd1HTQMS)) #For Full results, see SI
for i in inds:
    temp=Pd1HTQMS[i]
    lab=None

    lab=str(Pd1HTTemps[i])+"$^oC$"
    QMSValMean = Pd1HTPred1[i,1:alphaX]

    QMSValMean= RollQMS(QMSValMean,3)
    ax2.plot(np.flip(alpha[1:alphaX]),QMSValMean,c=pd1DAEC(c1-i*(c1-c3)/len(Pd1HTQMS)),label=lab,alpha=1)
    saveMainPredX=np.append(saveMainPredX,1-alpha[np.argmax(QMSValMean)+1])
    saveMainPredY=np.append(saveMainPredY,Pd1HTTemps[i])
    
    ax2.scatter(1-alpha[np.argmax(QMSValMean)+1],np.max(QMSValMean),alpha=a,s=s,marker=marker,c=pd1DAEC(c1-i*(c1-c3)/len(Pd1HTQMS)))
    ax2Max.scatter(1-alpha[np.argmax(QMSValMean)+1],Pd1HTTemps[i],c=pd1DAEC(c1-i*(c1-c3)/len(Pd1HTQMS)),alpha=a,s=int(s/2),marker=marker)
    
ax2.set_xlabel(r'$α=P_{CO}/(P_{O2}+P_{CO})$')
ax2.set_xticklabels(np.flip(ax2.get_xticklabels()))
rect=Rectangle((0,260),1,130,color="black",alpha=0.2)
ax2Max.add_patch(rect)
rect=Rectangle((0,0),1,1,color="black",alpha=0.2)
ax2.add_patch(rect)
for i in range(0,len(Pd0HTQMS)):
    temp=Pd0HTQMS[i]
    lab=None
    if i==0 :
        lab="$450^oC$"
    QMSValMean = Pd0HTPred1[i,1:alphaX]

    QMSValMean= RollQMS(QMSValMean,3)
    ax2.plot(np.flip(alpha[1:alphaX]),QMSValMean,c="black",label=lab,alpha=1)
    ax2.scatter(1-alpha[np.argmax(QMSValMean)+1],np.max(QMSValMean),c="black",alpha=a,s=s,marker=marker)
    saveMainPredX=np.append(saveMainPredX,1-alpha[np.argmax(QMSValMean)+1])
    saveMainPredY=np.append(saveMainPredY,Pd0HTTemps[i])
    
ax3Max.set_xticklabels(np.flip(ax3Max.get_xticklabels()))
labelLines(ax2.get_lines()[2:], zorder=2.5,backgroundcolor="none",align=False,yoffsets=1)#,xvals=saveMainPredX,shrink_factor=0.5)   
ax2.set_ylim(-1,10)    

l1=ax2.plot(0,-100,c=pd1DAEC(c2),label="Pd1 DAE")
l2=ax2.plot(0,-100,c="black",label="Pd0 DAE")
handles,labels=ax2.get_legend_handles_labels()
ax2.legend(handles[-2:],labels[-2:],loc="upper left")

ax3=plt.subplot(gs[-1:,0])
maxAlphaPd1 =1-alphaPlot[np.argmax(Pd1HTPred1[:,1:alphaX],1)]
ax3.set_ylabel(r'$α_{CO}^{max}$')
ax3.scatter([0]*6,1-maxAlphaPd1000[:6],c=pd1000C(c1),s=int(s/4),alpha=0.5)
ax3.scatter([1]*6,1-maxAlphaPd10[:6],c=pd10DAEC(c1),s=int(s/4),alpha=0.5)
ax3.scatter([2]*3,1-maxAlphaPd1[inds[:3]],c=pd1DAEC(c1),s=int(s/4),alpha=0.5)

ax3.set_xticks([0,1,2])
ax3.set_xticklabels(["Pd1000","Pd10","Pd1"])
ax3.plot([-1,3],[0.65,0.65],c="black",alpha=0.3)
ax3.set_xlim(-0.1,2.1)
ax3.set_ylim(0.45,0.8)


ax4=plt.subplot(gs[-1:,1])
maxPd1 =np.max(Pd1HTPred1[:,1:alphaX],1)
ax4.set_ylabel("Counts/particle\n @\n"+r'$α_{CO}^{max}$')
ax4.scatter([0]*6,maxPd1000[:6]/1000,c=pd1000C(c1),s=int(s/4),alpha=0.5)
ax4.scatter([1]*6,maxPd10[:6]/10,c=pd10DAEC(c1),s=int(s/4),alpha=0.5)
ax4.scatter([2]*3,maxPd1[inds[:3]],c=pd1DAEC(c1),s=int(s/4),alpha=0.5)
ax4.set_xticks([0,1,2])
ax4.set_xticklabels(["Pd1000","Pd10","Pd1"])
ax4.plot([-1,3],[5,5],c="black",alpha=0.3)
ax4.set_ylim(0,10)
ax4.set_xlim(-0.1,2.1)

x1=-0.2
y1=1
ax1H1.annotate("A", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2H1.annotate("B", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)


ax1L1.annotate("C", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2L1.annotate("D", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)

ax1.annotate("E", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)
ax2.annotate("F", xy=(x1, y1), xycoords="axes fraction",fontsize=titleFS)

ax3.annotate("G", xy=(x1, 2), xycoords="axes fraction",fontsize=titleFS)
ax4.annotate("H", xy=(x1, 2), xycoords="axes fraction",fontsize=titleFS)

fig.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=0.35, hspace=0.3)