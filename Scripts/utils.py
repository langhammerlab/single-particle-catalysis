import numpy as np
import os
import pandas as pd
def readQMS(dataPath,shiftBaseline = True):
    files = os.listdir(dataPath)
    for fileName in files:
        
        if fileName.endswith(".txt"):

    
            QMSInfo = np.loadtxt(dataPath+"/"+fileName,skiprows=11,usecols=[0,2,3,4])
            MFCInfo = np.loadtxt(dataPath+"/"+fileName,skiprows=11,usecols=[2,3,6,7]) #cols 2,3 is input to MFC, 6,7 is read from MFC
            inputTime = QMSInfo[:,0]
            O2Input=QMSInfo[:,2]
            COInput=QMSInfo[:,3]
            gasInput = np.sum(QMSInfo[:,1:],1)  
            gasInput[gasInput!=0] = 1
            temp = np.diff(gasInput)
            if "Chip_E_Pd1000" not in fileName and "Chip_F_Pd100" not in fileName:
                timeOfPulseStart = np.where(temp!=0)[0]*1000
            else:
                timeOfPulseStart = np.where(temp!=0)[0]*10*1000 #time in ms. does not work on Chip B
            
            timeOfPulseStart = np.append(0,timeOfPulseStart)
            
            

            
    
        if fileName.endswith("CO2.csv"):
            
            QMS = np.loadtxt(dataPath+"/"+fileName,skiprows=29,usecols=[1,2],delimiter = ";")  
            
        if fileName.endswith("CO.csv"):
            
            QMSCO = np.loadtxt(dataPath+"/"+fileName,skiprows=29,usecols=[1,2],delimiter = ";")  
            
        if fileName.endswith("O2.csv"):
            
            QMSO2 = np.loadtxt(dataPath+"/"+fileName,skiprows=29,usecols=[1,2],delimiter = ";")  
            
    if shiftBaseline:
        try:
            del indicesOfGas
            del indicesOfNoGas
            del partIndicesOfGas
            del partIndicesOfNoGas
            del clippedIndicesOfGas
            del clippedIndicesOfNoGas
            del alphaArray
        except:
            pass
        shiftedQMS = np.copy(QMS[:,1])
        meanQMS = np.copy(QMS[:,1])
        shiftedQMSCO = np.copy(QMSCO[:,1])
        shiftedQMSO2 = np.copy(QMSO2[:,1])

        
        if np.max(timeOfPulseStart)<10**7: 
            timeOfPulseStart = timeOfPulseStart*10

        for i in range(0,len(timeOfPulseStart)-1):
             
             QMStime = np.copy(QMS[:,0])
             QMStime[QMStime < timeOfPulseStart[i]] = 0
             QMStime[QMStime > timeOfPulseStart[i+1]] = 0
             QMStime[QMStime != 0] = 1
             indices = np.where(QMStime!=0)[0]
             
             indicesCutoff = 5
              #Assume that first pulse is no gas, rest is periodically gas-nogas
             if shiftBaseline and not i % 2:
               x = np.linspace(0,len(indices[indicesCutoff:-indicesCutoff]),len(indices[indicesCutoff:-indicesCutoff]))
               fit =  np.polynomial.polynomial.Polynomial.fit(x,QMS[indices[indicesCutoff:-indicesCutoff],1],1)
               fitCO = np.polynomial.polynomial.Polynomial.fit(x,QMSCO[indices[indicesCutoff:-indicesCutoff],1],1)
               fitO2 = np.polynomial.polynomial.Polynomial.fit(x,QMSO2[indices[indicesCutoff:-indicesCutoff],1],1)
            
             if shiftBaseline:
                 fit.convert(domain=[0,len(indices)])
                 fittedLine = fit.linspace(np.size(indices))
                 shiftedQMS[indices] = shiftedQMS[indices]-fittedLine[1]
                 fitCO.convert(domain=[0,len(indices)])
                 fittedLineCO = fitCO.linspace(np.size(indices))
                 shiftedQMSCO[indices] = shiftedQMSCO[indices]-fittedLineCO[1]
                 fitO2.convert(domain=[0,len(indices)])
                 fittedLineO2 = fitO2.linspace(np.size(indices))
                 shiftedQMSO2[indices] = shiftedQMSO2[indices]-fittedLineO2[1]
                                       
             if not i % 2:
                 try:
                     indicesOfNoGas = np.append(indicesOfNoGas,indices)
                     partIndicesOfNoGas = np.append(partIndicesOfNoGas,indices[0])
                 except:
                     indicesOfNoGas = indices
                     partIndicesOfNoGas = indices[0]
                     
                 try:
                     clippedIndicesOfNoGas = np.append(clippedIndicesOfNoGas,indices[indicesCutoff:-indicesCutoff])
                 except:
                     clippedIndicesOfNoGas = indices[indicesCutoff:-indicesCutoff]
    
                
             else:
                try:
                     indicesOfGas = np.append(indicesOfGas,indices)
                     partIndicesOfGas = np.append(partIndicesOfGas, indices[0])
                except:
                     indicesOfGas = indices   
                     partIndicesOfGas = indices[0]  
                try:
                     clippedIndicesOfGas = np.append(clippedIndicesOfGas,indices[indicesCutoff:-indicesCutoff])
                     alphaArray = np.append(alphaArray,np.mean(shiftedQMS[indices[indicesCutoff:-indicesCutoff]]))
                except:
                     clippedIndicesOfGas = indices[indicesCutoff:-indicesCutoff]
                     alphaArray = np.mean(shiftedQMS[indices[indicesCutoff:-indicesCutoff]])
                     
    
                     
             meanQMS[indices] = np.mean(shiftedQMS[indices[indicesCutoff:-indicesCutoff]])
    

        QMS = QMS[:indices[-1],:]
        shiftedQMS = shiftedQMS[:indices[-1]]
        meanQMS = meanQMS[:indices[-1]]
        shiftedQMSCO = shiftedQMSCO[:indices[-1]]
        shiftedQMSO2 = shiftedQMSO2[:indices[-1]]
        QMSCO = QMSCO[:indices[-1],:]
        QMSO2 = QMSO2[:indices[-1],:]
        shiftedQMS[shiftedQMS>3*(np.std(shiftedQMS)+np.mean(shiftedQMS))] = np.mean(shiftedQMS)
        shiftedQMS[shiftedQMS<(-3*np.std(shiftedQMS)-np.mean(shiftedQMS))] = np.mean(shiftedQMS)
    rawQMS = np.copy(QMS) 

    if indicesOfGas[-1] == len(QMS):
        indicesOfGas = indicesOfGas[:-1]
    if indicesOfNoGas[-1] == len(QMS):
        indicesOfNoGas = indicesOfNoGas[:-1]
        

    if shiftBaseline:
        output = QMS[:,1], shiftedQMS, meanQMS,alphaArray, indicesOfGas, indicesOfNoGas, clippedIndicesOfGas, clippedIndicesOfNoGas, QMS[:,0], rawQMS[:,1],partIndicesOfGas,partIndicesOfNoGas,shiftedQMSCO, shiftedQMSO2,O2Input,COInput
    else:
        output = rawQMS[:,1], QMSCO[:,1], QMSO2[:,1]
    
    return output

def readQMS_new(dataPath,shiftBaseline = True, indicesCutoff = 5):
    files = os.listdir(dataPath)
    for fileName in files:
        
        if fileName.endswith(".txt"):

    
            QMSInfo = np.loadtxt(dataPath+"/"+fileName,skiprows=11,usecols=[0,2,3,4])
            inputTime = QMSInfo[:,0]
            gasInput = np.sum(QMSInfo[:,1:],1)  
            gasInput[gasInput!=0] = 1
            temp = np.diff(gasInput)
            if "Chip_E_Pd1000" not in fileName and "Chip_F_Pd100" not in fileName:
                timeOfPulseStart = np.where(temp!=0)[0]*1000
            else:
                timeOfPulseStart = np.where(temp!=0)[0]*10*1000 #time in ms. does not work on Chip B
            
            timeOfPulseStart = np.append(0,timeOfPulseStart)
            
            

            
    
        if fileName.endswith("CO2.csv"):
            
            QMS = np.loadtxt(dataPath+"/"+fileName,skiprows=29,usecols=[1,2],delimiter = ";")  
            
        if fileName.endswith("CO.csv"):
            
            QMSCO = np.loadtxt(dataPath+"/"+fileName,skiprows=29,usecols=[1,2],delimiter = ";")  
            
        if fileName.endswith("O2.csv"):
            
            QMSO2 = np.loadtxt(dataPath+"/"+fileName,skiprows=29,usecols=[1,2],delimiter = ";")  
            
    if shiftBaseline:
        try:
            del indicesOfGas
            del indicesOfNoGas
            del partIndicesOfGas
            del partIndicesOfNoGas
            del clippedIndicesOfGas
            del clippedIndicesOfNoGas
            del alphaArray
        except:
            pass

        if np.max(timeOfPulseStart)<10**7:
            timeOfPulseStart = timeOfPulseStart*10

        for i in range(0,len(timeOfPulseStart)-1):
             
             QMStime = np.copy(QMS[:,0])
             QMStime[QMStime < timeOfPulseStart[i]] = 0
             QMStime[QMStime > timeOfPulseStart[i+1]] = 0
             QMStime[QMStime != 0] = 1
             indices = np.where(QMStime!=0)[0]           
            
             if not i % 2:
                 try:
                     indicesOfNoGas = np.append(indicesOfNoGas,indices)
                     partIndicesOfNoGas = np.append(partIndicesOfNoGas,indices[0])
                 except:
                     indicesOfNoGas = indices
                     partIndicesOfNoGas = indices[0]
                     
                 try:
                     clippedIndicesOfNoGas = np.append(clippedIndicesOfNoGas,indices[indicesCutoff:-indicesCutoff])
                 except:
                     clippedIndicesOfNoGas = indices[indicesCutoff:-indicesCutoff]
    
                
             else:
                try:
                     indicesOfGas = np.append(indicesOfGas,indices)
                     partIndicesOfGas = np.append(partIndicesOfGas, indices[0])
                except:
                     indicesOfGas = indices   
                     partIndicesOfGas = indices[0]  
                try:
                     clippedIndicesOfGas = np.append(clippedIndicesOfGas,indices[indicesCutoff:-indicesCutoff])
                except:
                     clippedIndicesOfGas = indices[indicesCutoff:-indicesCutoff]

                                            
        QMS = QMS[:indices[-1],:]       
        shiftedQMS = ShiftBaseline(QMS,partIndicesOfGas,partIndicesOfNoGas,indicesCutoff)
        shiftedQMS[shiftedQMS>3*(np.std(shiftedQMS)+np.mean(shiftedQMS))] = np.mean(shiftedQMS)
        shiftedQMS[shiftedQMS<(-3*np.std(shiftedQMS)-np.mean(shiftedQMS))] = np.mean(shiftedQMS)
        
        meanQMS = getMeanValue(shiftedQMS, partIndicesOfGas, partIndicesOfNoGas, indicesCutoff)

    rawQMS = np.copy(QMS) 

    if indicesOfGas[-1] == len(QMS):
        indicesOfGas = indicesOfGas[:-1]
    if indicesOfNoGas[-1] == len(QMS):
        indicesOfNoGas = indicesOfNoGas[:-1]
        

    if shiftBaseline:
        output = QMS[:,1], shiftedQMS, meanQMS,[0], indicesOfGas, indicesOfNoGas, clippedIndicesOfGas, clippedIndicesOfNoGas, QMS[:,0], rawQMS[:,1],partIndicesOfGas,partIndicesOfNoGas,QMSCO[:,1], QMSO2[:,1],MFCInfo
    else:
        output = rawQMS[:,1], QMSCO[:,1], QMSO2[:,1]
    
    return output



def ShiftBaseline(QMS,partIndicesOfGas,partIndicesOfNoGas,indicesCutOff,simulatedConstant=False):
    try:
        shiftQMS = np.copy(QMS[:,1])
    except:
        shiftQMS = np.copy(QMS)
    for i in range(0,len(partIndicesOfGas)-1):
        indicesOfNoGas = np.append(np.arange(partIndicesOfNoGas[i],partIndicesOfGas[i]),np.arange(partIndicesOfNoGas[i+1],partIndicesOfGas[i+1]))
        clippedIndicesOfNoGas =  np.append(np.arange(partIndicesOfNoGas[i]+indicesCutOff,partIndicesOfGas[i]-indicesCutOff),np.arange(partIndicesOfNoGas[i+1]+indicesCutOff,partIndicesOfGas[i+1]-indicesCutOff))
        
        x = np.arange(0,len(clippedIndicesOfNoGas))
        fit =  np.polynomial.polynomial.Polynomial.fit(x,shiftQMS[clippedIndicesOfNoGas],1)
        
        indices = np.arange(indicesOfNoGas[0],partIndicesOfNoGas[i+1])
        
        fit.convert(domain=[0,len(indices)])
        fittedLine = fit.linspace(np.size(indices))
        shiftQMS[indices] = shiftQMS[indices]-fittedLine[1]
        if simulatedConstant:
            shiftQMS[indices]*=1+np.random.rand()*simulatedConstant
            
  
    indices = np.arange(indices[-1]+1,len(QMS))
    
    fit.convert(domain=[0,len(indices)])
    fittedLine = fit.linspace(np.size(indices))
    shiftQMS[indices] = shiftQMS[indices]-fittedLine[1]
    if simulatedConstant:
        shiftQMS[indices]*=1+np.random.rand()*simulatedConstant
    
    return shiftQMS

def RollQMS(QMSVal,rollingWindow=5):
    if rollingWindow<=1:
        return QMSVal
    pad = np.zeros((31))
    pad[5:-5] =QMSVal
    QMSVal=pad
    QMSVal = pd.DataFrame(QMSVal).rolling(rollingWindow,center=True).mean().to_numpy()
    QMSVal = QMSVal[6:-4]
    #QMSVal = np.array([int(v) for v in QMSVal])
    return QMSVal

def setLastIndicestoZero(QMS):
    #remove influence of unused portion of experiments on prediction
    QMS[:,43*128:]=0
    return QMS


def setFirstIndicestoZero(QMS):
    #remove influence of calibration sequence on prediction
    try:
        QMS[:,:250]=0
    except:
        QMS[:250]=0
    return QMS


def baseline_arPLS(y, ratio=1e-6, lam=100, niter=10, full_output=False):
    import scipy as sp
    L = len(y)
    diag = np.ones(L-2)
    D = sp.sparse.spdiags([diag, -2*diag, diag], [0, -1, -2], L, L-2)
    H = lam * D.dot(D.T)
    w = np.ones(L)
    W = sp.sparse.spdiags(w, 0, L, L)
    crit = 1
    count = 0
    while crit > ratio:
        z = sp.sparse.linalg.spsolve(W + H, W*y)
        d = y - z
        dn = d[d < 0]
        m = np.mean(dn)
        s = np.std(dn)
        w_new = 1/(1 + np.exp(2*(d - (2*s - m))/s))
        crit = sp.linalg.norm(w_new - w)/sp.linalg.norm(w)
        w = w_new
        W.setdiag(w) # Do not create a new matrix, just update diagonal values
        count += 1
        if count > niter:
            print('Maximum number of iterations exceeded')
            break
    if full_output:
        info = {'num_iter': count, 'stop_criterion': crit}
        return z, d, info
    else:
        return z
    
    
def normalizebymaxAlpha(noise,signal=None):
    constant = 100
    try:
        signal = signal/constant
    except:
        pass
    noise = noise/constant
    
    return noise,signal,constant

def getfullIndicesOfGas(indicesOfGas,indicesOfNoGas):

    fullIndicesOfGas = np.array([],dtype=int)
    fullIndicesOfNoGas = np.array([],dtype=int)
    for i in range(0,len(indicesOfNoGas)-1):
        newIndices=np.arange(indicesOfNoGas[i],indicesOfGas[i],dtype=int)
        fullIndicesOfNoGas = np.append(fullIndicesOfNoGas,newIndices)
        newIndices=np.arange(indicesOfGas[i],indicesOfNoGas[i+1],dtype=int)
        fullIndicesOfGas = np.append(fullIndicesOfGas,newIndices)
        
    return fullIndicesOfGas,fullIndicesOfNoGas   

def getMeanValue(QMS,partIndicesOfGas,partIndicesOfNoGas,indicesCutOff=3):
    meanQMS=np.copy(QMS)
    for i in range(0,np.size(partIndicesOfNoGas)-1):
        meanQMS[partIndicesOfGas[i]:partIndicesOfNoGas[i+1]] = np.mean(QMS[partIndicesOfGas[i]+indicesCutOff:partIndicesOfNoGas[i+1]-indicesCutOff])   
        meanQMS[partIndicesOfNoGas[i]:partIndicesOfGas[i]] = np.mean(QMS[partIndicesOfNoGas[i]+indicesCutOff:partIndicesOfGas[i]-indicesCutOff])  

    meanQMS[partIndicesOfNoGas[-1]:] = np.mean(meanQMS[partIndicesOfNoGas[-1]:])
    return meanQMS


def getQMSInput(inputGas,partIndicesOfGas,partIndicesOfNoGas):
    meanQMS=np.zeros((2,6400))
    indices = np.append(partIndicesOfNoGas,partIndicesOfGas)
    indices=np.sort(indices)
    for i in range(0,len(indices)-1):
        meanQMS[0,indices[i]:indices[i+1]] = inputGas[i,0]
        meanQMS[1,indices[i]:indices[i+1]] = inputGas[i,1]

    return meanQMS

def getAlphaValue(QMS,partIndicesOfGas,partIndicesOfNoGas,indicesCutOff=3,median=False,std=False):
    alphaQMS=np.zeros((len(partIndicesOfGas)-1))
    
    for i in range(1,np.size(partIndicesOfGas)-1):
        if median:
            alphaQMS[i] = np.median(QMS[partIndicesOfGas[i]+indicesCutOff:partIndicesOfNoGas[i+1]-indicesCutOff])   
        if std:
            alphaQMS[i] = np.std(QMS[partIndicesOfGas[i]+indicesCutOff:partIndicesOfNoGas[i+1]-indicesCutOff])   
        else:
            alphaQMS[i] = np.mean(QMS[partIndicesOfGas[i]+indicesCutOff:partIndicesOfNoGas[i+1]-indicesCutOff])   

    if median:
        alphaQMS[-1] = np.median(QMS[partIndicesOfNoGas[-1]:])
    if std:
        alphaQMS[-1] = np.std(QMS[partIndicesOfNoGas[-1]:])
    else:
        alphaQMS[-1] = np.mean(QMS[partIndicesOfNoGas[-1]:])
    return alphaQMS

def SplitIntoPeaks(QMS,partIndicesOfGas,partIndicesOfNoGas,windowLen=200):
    splitQMS=np.zeros((windowLen,len(partIndicesOfGas)-1))
    for i in range(0,np.size(partIndicesOfGas)-1):   
        midIndex= partIndicesOfGas[i] + int((partIndicesOfNoGas[i+1]-partIndicesOfGas[i])/2)
        splitQMS[:,i] = QMS[midIndex-int(windowLen/2):midIndex+int(windowLen/2)]  

    return splitQMS
    
    