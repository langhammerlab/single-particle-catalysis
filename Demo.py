#%% Preamble
import os
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers, Input, Model
from tensorflow.keras.layers import LeakyReLU, Conv1D, MaxPooling1D, Flatten, Dense, Lambda, Reshape, Concatenate, UpSampling1D
from tensorflow.keras.activations import relu
from tensorflow.keras.losses import MeanAbsoluteError
from tensorflow.keras.optimizers import Adam
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import seaborn as sns

from Scripts.utils import *

part_indices_of_gas = np.loadtxt("scripts/GasIndices.txt", dtype=int)
part_indices_of_no_gas = np.loadtxt("scripts/ArIndices.txt", dtype=int)

#%% Define Auto-Encoder

def get_cdae(input_shape=(6400,1),smallest_dim=50,neurons = 32, kernel_size = 9,activation_function = LeakyReLU()):
    inputs = Input(shape=input_shape)
    x = inputs
    for _ in range(6):
        x = Conv1D(neurons, kernel_size, activation=activation_function, padding='same')(x)
        x = MaxPooling1D(2, padding='same')(x)
        
    x = Conv1D(neurons, kernel_size, activation=activation_function, padding='same')(x)
    encoded = MaxPooling1D(2, padding='same')(x)
    vals = Flatten()(encoded)
    vals = Dense(smallest_dim)(vals)
    vals = Lambda(lambda x: relu(x))(vals)
    x = Reshape((smallest_dim, 1))(vals)
    x = Concatenate()([x, encoded])

    for _ in range(7):
        x = Conv1D(neurons, kernel_size, activation=activation_function, padding='same')(x)
        x = UpSampling1D(2)(x)

    decoded = Conv1D(1, kernel_size, activation=activation_function, padding='same')(x)
    
    return Model(inputs, [decoded, vals])

def consistency_loss(true, pred):
    signals = tf.reshape(pred, [-1, 6400])
    values = true[:, :43]
    indices_cutoff = 3
    pred_values = []
    for j in range(signals.shape[0]):
        for i in range(len(part_indices_of_gas) - 1):
            pred_values.append(tf.reduce_mean(signals[j, part_indices_of_gas[i] + indices_cutoff:part_indices_of_no_gas[i + 1] - indices_cutoff]))
        pred_values.append(tf.reduce_mean(signals[j, part_indices_of_no_gas[-1]:]))
    pred_values = tf.stack(pred_values)
    pred_values = tf.reshape(pred_values, [-1, 43])

    return MeanAbsoluteError()(values, pred_values)

def weighted_mae(true, pred):
    true, pred = true[:, :43], pred[:, :43]
    return MeanAbsoluteError()(true, pred)

optimizer = Adam(learning_rate=1e-6, beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False)

smallest_dim = 50
autoencoder = get_cdae(smallest_dim=smallest_dim)
losses = ["mae", weighted_mae, consistency_loss]
autoencoder.load_weights('Weights/autoencoder-ks9-230402.h5')
autoencoder.compile(optimizer="adam", loss=losses)


#%% Perform inference
PO = [0,0.75,0.8]
PO = np.append(PO,np.arange(0.9,6.3,0.3))
PCO = np.flip(np.append([0,0.4],np.arange(0.6,6.3,0.3)))
PO = np.append(PO,np.flip(PO))
PCO = np.append(PCO,np.flip(PCO))
alpha = PCO/(PO+PCO)
alphaPlot = alpha[1:22]

C = get_cmap("Blues")

Pd10=1
Pd1=0
Pd0=0

save=0
indicesCutOff=3

if Pd10:
    test_path="Data/Chip D Pd10/"
elif Pd1:
    test_path="Data/Chip K Pd1/"
elif Pd0:
    test_path="Data/Chip P Pd0/"
    
test_files = np.array(os.listdir(test_path))
nrOfFiles=len(test_files)
temperature = np.zeros(len(test_files))

for i in range(0,len(test_files)):
    try:
        temperature[i] = int(test_files[i][test_files[i].index("QMS_")+4:test_files[i].index("oC")])
    except:
        temperature[i]=0
        
sortedInd = np.flip(np.argsort(temperature))
test_files = test_files[sortedInd]
temperature=temperature[sortedInd]

sns.set(font_scale=1)
fig,axs=plt.subplots(3,3,figsize=(16,16))

axs=axs.flatten()
counter=1
QMSByConc = []
QMSByConcAlt = []
QMSByMean = []
QMSFull = np.zeros((len(test_files),6400))
QMSPredFull = np.zeros((len(test_files),6400))
temperatures=[]
labels=[]
maxAlpha1=[]
maxAlpha2=[]
for i in range(0,len(test_files)):
    if i >= 9*counter:
        counter+=1
        fig,axs=plt.subplots(3,3,figsize=(16,16))
        axs=axs.flatten()
    ax = axs[i-9*(counter-1)]
    
    try:
        test = readQMS(test_path+"/"+test_files[i])
    except:
        print(test_files[i]+ "failed")
        continue

    indices_of_gas_test = test[5]
    indices_of_ar_test = test[6]
    indices_of_gas_test,indices_of_ar_test = indices_of_ar_test, indices_of_gas_test
    part_indices_of_gas_test = test[10]
    part_indices_of_ar_test = test[11]
    if len(part_indices_of_gas_test) != 43: #hacky fix for the prematurely cancelled 360OC Measurement
            part_indices_of_gas_test=part_indices_of_gas
            part_indices_of_ar_test=part_indices_of_no_gas

    meanTest = test[2]
    test = test[1]
    test=setFirstIndicestoZero(test)
    nptsTest = len(test)
    QMSFull[i,:nptsTest] = test
    test,_,constant=normalizebymaxAlpha(test)
    meanTest = getMeanValue(test,part_indices_of_gas_test,part_indices_of_ar_test,indicesCutOff=indicesCutOff)

    ax.plot(test*constant,label='Raw QMS Output')
    ax.plot(meanTest*constant,label='Mean QMS Output')

    temp = np.zeros(6400)
    temp[0:nptsTest] = test
    test = temp
    
    pred = autoencoder.predict(test.reshape(1,-1,1))
    predAlt = np.array(pred[1]*100)
    pred=np.array(pred[0])

    QMSPredFull[i,:nptsTest] = pred[0,:nptsTest,0]*constant
    if i != 0:
        ax.axes.get_xaxis().set_visible(False)
    if i == 0:
        ax.legend(loc='best')
        ax.set_ylabel('Normalized QMS Signal')
        ax.set_xlabel('Time (h)')
        
    alphaQMS = getAlphaValue(pred[0,:nptsTest,:]*constant,part_indices_of_gas_test,part_indices_of_ar_test)
    temp = np.zeros((43))
    temp[:len(alphaQMS)]=alphaQMS
    ax.plot(pred[0,:nptsTest,:]*constant,label='Predicted Signal')
    ax.scatter(part_indices_of_gas_test,predAlt[0,:43],c="red",marker="x",s=64)

    ax.set_title(test_files[i])
    QMSByConc =np.append(QMSByConc,temp)
    maxAlpha1 = np.append(maxAlpha1,np.argmax(temp[1:22]))
    alphaQMS = getAlphaValue(meanTest*constant,part_indices_of_gas_test,part_indices_of_ar_test)
    temp = np.zeros((43))
    temp[:len(alphaQMS)]=alphaQMS
    QMSByMean=np.append(QMSByMean,temp)
    QMSByConcAlt =np.append(QMSByConcAlt, predAlt[0,:43])
    temperatures=np.append(temperatures,int(test_files[i][test_files[i].index("QMS_")+4:test_files[i].index("oC")]))
    labels=np.append(labels,test_files[i][:6])
    maxAlpha2 = np.append(maxAlpha2,np.argmax(predAlt[0,1:22]))
    
QMSByMean=QMSByMean.reshape(-1,43)
QMSByConc=QMSByConc.reshape(-1,43)
QMSByConcAlt=QMSByConcAlt.reshape(-1,43)  

maxAlpha1=[int(m) for m in maxAlpha1]
maxAlpha2=[int(m) for m in maxAlpha2]
alphaPlot=np.flip(alphaPlot)

plt.legend(loc="best")
plt.figure()
for i in range(0,len(temperatures)): 
    plt.plot(alphaPlot,QMSByConc[i,1:22],c=C(0.9-i*(0.8)/len(temperatures)),label=labels[i],alpha=1)
    plt.scatter(alphaPlot[np.argmax(QMSByConc[i,1:22])],np.max(QMSByConc[i,1:22]),c="red",alpha=1,s=128)


if Pd10 and save:
    
    Pd10QMS  = np.save("Results/Pd10-QMS.npy",QMSByMean)
    Pd10Pred1 = np.save("Results/Pd10-Pred1.npy",QMSByConc)
    Pd10Pred2 = np.save("Results/Pd10-Pred2.npy",QMSByConcAlt)
    Pd10PredFull = np.save("Results/Pd10-PredFull.npy",QMSPredFull)
    Pd10Temps = np.save("Results/Pd10-Temps.npy",temperatures)
    Pd10QMS = np.save("Results/Pd10-QMS.npy",QMSFull)
    
    
if Pd1 and save:
    Pd0QMSFull  = np.save("Results/Pd1-QMS-Full.npy",QMSFull)
    Pd1QMS  = np.save("Results/Pd1-QMS.npy",QMSByMean)
    Pd1Pred1 = np.save("Results/Pd1-Pred1.npy",QMSByConc)
    Pd1Pred2 = np.save("Results/Pd1-Pred2.npy",QMSByConcAlt)
    Pd1Temps = np.save("Results/Pd1-Temps.npy",temperatures)


if Pd0 and save:
    Pd0QMSFull  = np.save("Results/Pd0-QMS-Full.npy",QMSFull)
    Pd0QMS  = np.save("Results/Pd0-QMS.npy",QMSByMean)
    Pd0Pred1 = np.save("Results/Pd0-Pred1.npy",QMSByConc)
    Pd0Pred2 = np.save("Results/Pd0-Pred2.npy",QMSByConcAlt)
    Pd0Temps = np.save("Results/Pd0-Temps.npy",temperatures)

